package com.linln.modules.system.repository;

import com.linln.modules.system.domain.ScheduledTask;
import com.linln.modules.system.repository.BaseRepository;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/04/15
 */
public interface ScheduledTaskRepository extends BaseRepository<ScheduledTask, Long> {



            ScheduledTask findByTaskKey(String teskKey);


            List<ScheduledTask> findAllByInitStartFlag(int initStartFlag);
}