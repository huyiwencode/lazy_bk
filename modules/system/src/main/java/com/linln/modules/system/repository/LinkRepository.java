package com.linln.modules.system.repository;

import com.linln.modules.system.domain.Link;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author 小懒虫
 * @date 2020/02/14
 */
public interface LinkRepository extends BaseRepository<Link, Long> {
}