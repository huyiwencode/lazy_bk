package com.linln.modules.system.repository;

import com.linln.modules.system.domain.Art;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author 小懒虫
 * @date 2020/02/14
 */
public interface ArtRepository extends BaseRepository<Art, Long> {
}