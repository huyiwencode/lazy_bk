package com.linln.modules.system.domain;

import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.StatusUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * @author 小懒虫
 * @date 2020/04/19
 */
@Data
@Entity
@Table(name="or_mail")
@Where(clause = StatusUtil.NOT_DELETE)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Mail implements Serializable {
    // 主键ID
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    // 邮件ID
    private  int messageNumber;
    // 主题
    private String subject;
    // 发件人
    private String fromAddress;
    // 收件人
    private String receiveAddress;
    // 发送时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date sentDate;
    // 是否已读
    private Boolean isSeen;
    // 邮件优先级
    private String priority;
    // 是否需要回执
    private Boolean isReplySign;
    // 邮件大小
    private int size;
    // 是否包含附件
    private Boolean isContainerAttachment;
    // 邮件正文
    @Lob
    @Column(columnDefinition="TEXT")
    private String content;
    // 备注
    private String remark;
    // 创建时间
    private Date createDate;
    // 更新时间
    private Date updateDate;
    // 数据状态
    private Byte status = StatusEnum.OK.getCode();
}