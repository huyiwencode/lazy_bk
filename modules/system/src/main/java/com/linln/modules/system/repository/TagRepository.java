package com.linln.modules.system.repository;

import com.linln.modules.system.domain.Tag;
import com.linln.modules.system.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/01/01
 */
public interface TagRepository extends BaseRepository<Tag, Long> {

    Tag findByName(String name);
    /**
     * 查询文章/笔记的相关标签，并selected
     *
     * @param referId
     * @param
     * @return
     */
    @Query(nativeQuery = true,
            value = "SELECT a.*, IF(COUNT(1) > 1, 'selected', '') AS selected" +
                    "        FROM ((SELECT t.* FROM nb_tag t)" +
                    "              UNION ALL" +
                    "              (SELECT t.*" +
                    "               FROM nb_tag t" +
                    "               WHERE t.id IN (SELECT tr.tag_id FROM nb_tag_refer tr WHERE tr.refer_id = ?1 ))) a" +
                    "        GROUP BY a.`name`")
    List<Object[]> findTagListSelected(Long referId);
}