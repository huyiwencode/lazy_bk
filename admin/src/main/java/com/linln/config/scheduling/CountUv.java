package com.linln.config.scheduling;

import com.linln.modules.system.domain.Param;
import com.linln.modules.system.repository.ParamRepository;
import com.linln.modules.system.repository.UvRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CountUv implements ScheduledTaskJob {

    @Autowired
    UvRepository uvRepository;
    @Autowired
    ParamRepository paramRepository;
    @Override
    public void run() {
        log.info("》》》》》》开始计算访客量");
        Param param =paramRepository.findFirstByName("visitor_counts");
        int newCount= uvRepository.findCountBefore();
        param.setValue(String.valueOf(Integer.valueOf(param.getValue())+newCount));
        paramRepository.save(param);
        log.info("》》》》》》结束计算访客量");
    }
}