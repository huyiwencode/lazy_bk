package com.linln.component.email;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Email {

    private  String host;
    private  String port;
    private  String from;
    private  String user;
    private  String pass;
    private  String to;
    private  String subject;
    private  String template;
    private  String comment;
}
